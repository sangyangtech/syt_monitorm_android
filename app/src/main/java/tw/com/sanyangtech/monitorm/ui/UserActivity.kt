package tw.com.sanyangtech.monitorm.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.TextView
import tw.com.sanyangtech.monitorm.R
import tw.com.sanyangtech.monitorm.dto.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.thread


class UserActivity : MonitorMActivity() {


    private val expiredDateTv by bind<TextView>(R.id.tv_expired_date)

    private val accountIndex: Int by lazy {
        this.intent.getIntExtra(EXTRA_ACCOUNT_INDEX, -1);
    }
    private val account: AccountDTO by lazy {
        GoogleDriveRepo.GetAccountList().get(accountIndex)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.setContentView(tw.com.sanyangtech.monitorm.R.layout.activity_user)

        // det expired date
        val expiredDateFormatter = SimpleDateFormat(EXPIRED_DATE_FORMAT)
        expiredDateTv.text = expiredDateFormatter.format(Date(account.expired))


        //toDo: get resource using thread
        thread(start = true) {
            GoogleDriveRepo.GetFaultReasons()
        }


        val supportFragmentManager = supportFragmentManager
        val fragmentTransaction = supportFragmentManager.beginTransaction()

        val fragment: Fragment
        if (account.repairState == REPAIR_STATE_STILL_FAULT || account.repairState == REPAIR_STATE_REPORTED) {
            fragment = FragmentFaultResolution()
        } else {
            fragment = FragmentReport()
        }

        val bundle = Bundle()
        bundle.putInt(EXTRA_ACCOUNT_INDEX, accountIndex)
        fragment.arguments = bundle
        fragmentTransaction.add(R.id.fragment_container, fragment)
        fragmentTransaction.commit()
    }
}
