package tw.com.sanyangtech.monitorm.uitls

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import android.util.TypedValue
import android.widget.TextView
import tw.com.sanyangtech.monitorm.R


fun showAlertDialog(context: Context, title: String?, content: String,
                    positiveBtStr: String, positiveListener: DialogInterface.OnClickListener,
                    negativeBtStr: String?, negativeListener: DialogInterface.OnClickListener?) {
    val alertDialogBuilder = AlertDialog.Builder(context)
    if (title != null) {
        alertDialogBuilder.setTitle(title)
    }
    alertDialogBuilder
        .setMessage(content)
        .setCancelable(false)
        .setPositiveButton(positiveBtStr, positiveListener)
    if (negativeBtStr != null && negativeListener != null) {
        alertDialogBuilder.setNegativeButton(negativeBtStr, negativeListener)
    }
    val alertDialog = alertDialogBuilder.create()
    alertDialog.show()
    alertDialog.findViewById<TextView>(android.R.id.message)
        ?.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen.font_title_size))
    alertDialog.getButton(Dialog.BUTTON_POSITIVE)
        ?.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen.font_title_size))
    alertDialog.getButton(Dialog.BUTTON_NEGATIVE)
        ?.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen.font_title_size))
}

class Log {
    companion object {
        var debugLevel = android.util.Log.INFO
        fun i (tag: String, msg: String?) {
            if(debugLevel <= android.util.Log.INFO) {
                android.util.Log.i(tag, msg)
            }
        }
        fun v (tag: String, msg: String?) {
            if(debugLevel <= android.util.Log.VERBOSE) {
                android.util.Log.v(tag, msg)
            }
        }

        fun e (tag: String, msg: String?) {
            if(debugLevel <= android.util.Log.ERROR) {
                android.util.Log.v(tag, msg)
            }
        }
        fun w (tag: String, msg: String?) {
            if(debugLevel <= android.util.Log.WARN) {
                android.util.Log.w(tag, msg)
            }
        }

    }
}