package tw.com.sanyangtech.monitorm.ui

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.ViewGroup
import android.widget.*
import tw.com.sanyangtech.monitorm.R
import tw.com.sanyangtech.monitorm.dto.ACCOUNT_INVALID
import tw.com.sanyangtech.monitorm.dto.AccountDTO
import tw.com.sanyangtech.monitorm.dto.GoogleDriveRepo
import tw.com.sanyangtech.monitorm.uitls.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.thread


class AdminActivity : MonitorMActivity() {


    private val operatorLayout by bind<View>(R.id.cs_header_operator)
    private val saveButton by bind<Button>(R.id.bt_header_save)
    private val returnButton by bind<Button>(R.id.bt_header_return)


    private val editorLayout by bind<View>(R.id.layout_body_editor)
    private val idEt by bind<EditText>(R.id.et_editor_account)
    private val pwdEt by bind<EditText>(R.id.et_editor_password)
    private val warrantyDate by bind<EditText>(R.id.et_editor_warranty)
    private val isInvalidCb by bind<CheckBox>(R.id.cb_isinvalid)
    private val commentEt by bind<EditText>(R.id.et_editor_comment)


    private val accountListView by bind<ListView>(R.id.lv_account)
    private val accountListLayout by bind<View>(R.id.layout_body_account_list)

    private val accountList = GoogleDriveRepo.GetAccountList()
    private val adapter by lazy { AccountAdapter(accountList.subList(1, accountList.size), this) }

    private var targetAccountIdx = -1
    private val handler = Handler(Looper.getMainLooper())


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.setContentView(R.layout.activity_admin)
        Log.i(TAG, accountList.subList(10, accountList.size).toString())

        accountListView.adapter = adapter
        val itemListener = AdapterView.OnItemClickListener { parent, view, idx, id ->
            // admin was excluded
            enterEditorMode(idx + 1)
        }
        accountListView.onItemClickListener = itemListener

        returnButton.setOnClickListener {
            enterListMode()
        }
        saveButton.setOnClickListener {

            val id = idEt.editableText.toString().trim()
            val pwd = pwdEt.editableText.toString().trim()
            val expiredDateUnix = try {
                SimpleDateFormat(EXPIRED_DATE_FORMAT).parse(warrantyDate.editableText.toString().trim())
            } catch (e: Exception) {
                -1
            }
            if (id.isEmpty()) {
                Toast.makeText(this, R.string.account_enter, Toast.LENGTH_LONG).show()
            } else if (pwd.isEmpty()) {
                Toast.makeText(this, R.string.password_enter, Toast.LENGTH_LONG).show()
            } else if (expiredDateUnix == -1) {
                Toast.makeText(this, R.string.date_format_incorrect, Toast.LENGTH_LONG).show()
            } else {
                thread(start = true) {
                    val account = accountList[targetAccountIdx]
                    val updatedResult = updateAccount(
                        account,
                        idEt.editableText.toString().trim(),
                        pwdEt.editableText.toString().trim(),
                        SimpleDateFormat(EXPIRED_DATE_FORMAT).parse(warrantyDate.editableText.toString().trim()).time,
                        if (isInvalidCb.isChecked) ACCOUNT_INVALID else 0,
                        commentEt.editableText.toString().trim(),
                        targetAccountIdx
                    )
                    if (updatedResult) {
                        handler.post {
                            enterListMode()
                        }
                    } else {
                        handler.post {
                            Toast.makeText(this@AdminActivity, R.string.save_failed, Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
        }

    }

    private fun enterEditorMode(idx: Int) {
        accountListLayout.visibility = View.INVISIBLE
        editorLayout.visibility = View.VISIBLE
        operatorLayout.visibility = View.VISIBLE
        setEditor(accountList.get(idx))
        targetAccountIdx = idx
    }

    private fun enterListMode() {
        accountListLayout.visibility = View.VISIBLE
        editorLayout.visibility = View.INVISIBLE
        operatorLayout.visibility = View.GONE
        adapter.notifyDataSetChanged()
    }


    private fun setEditor(account: AccountDTO) {
        idEt.setText(account.id)
        pwdEt.setText(account.pwd)
        warrantyDate.setText(SimpleDateFormat(EXPIRED_DATE_FORMAT).format(account.expired))
        isInvalidCb.isChecked = account.isInvalid == ACCOUNT_INVALID
        commentEt.setText(account.comment)

    }
}

private class AccountAdapter(val localAccountList: List<AccountDTO>, val context: Context) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var holder: ViewHolder
        var view: View
        if (convertView == null) {
            view = View.inflate(context, R.layout.item_account, null)
            holder = ViewHolder(view)
            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }
        holder.accountId.text = localAccountList[position].id
        holder.expiredDate.text = SimpleDateFormat(EXPIRED_DATE_FORMAT).format(Date(localAccountList[position].expired))
        return view
    }

    override fun getItem(position: Int) = localAccountList[position]


    override fun getItemId(position: Int) = position.toLong()

    override fun getCount() = localAccountList.size

}


class ViewHolder(var viewItem: View) {
    var accountId: TextView = viewItem.findViewById(R.id.tv_item_id) as TextView
    var expiredDate: TextView = viewItem.findViewById(R.id.tv_expired_date) as TextView
}
