package tw.com.sanyangtech.monitorm.dto



const val IDENTITY_ADMIN = "admin"
const val IDENTITY_USER = "user"
const val INVALID = 1

const val REPAIR_STATE_NORMAL = 0
const val REPAIR_STATE_REPORTED = 1
const val REPAIR_STATE_STILL_FAULT = 2


const val ACCOUNT_INVALID = 1

data class AccountDTO (var id: String = "",
                       var pwd: String = "",
                       var expired: Long = 0,
                       var isInvalid: Int = INVALID,
                       var repairState: Int = Int.MIN_VALUE,
                       var cDate: Long = 0,
                       var mDate: Long = 0,
                       var identity: String = "",
                       var comment: String = "")