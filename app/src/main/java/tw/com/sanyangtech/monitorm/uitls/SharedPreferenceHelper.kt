package tw.com.sanyangtech.monitorm.uitls

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager


private fun getSharedPreferenceObj(context: Context, preferenceName: String = ""): SharedPreferences {
    val sharedPreferences: SharedPreferences
    if (preferenceName.isEmpty()) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    } else {
        sharedPreferences = context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
    }
    return sharedPreferences
}

fun setSharedPreferences(context: Context, key: String, value: String, preferenceName: String = "") {
    val sharedPreferences = getSharedPreferenceObj(context, preferenceName)
    sharedPreferences.edit().putString(key, value).commit()
}

fun setSharedPreferences(context: Context, key: String, value: Int, preferenceName: String = "") {
    val sharedPreferences = getSharedPreferenceObj(context, preferenceName)
    sharedPreferences.edit().putInt(key, value).commit()
}

fun getSharedPreferences(context: Context, key: String, defValue: String, preferenceName: String = "" ): String {
    val sharedPreferences = getSharedPreferenceObj(context, preferenceName)
    return sharedPreferences.getString(key, defValue)
}

fun getSharedPreferences(context: Context, key: String, defValue: Int, preferenceName: String = ""): Int {
    val sharedPreferences = getSharedPreferenceObj(context, preferenceName)
    return sharedPreferences.getInt(key, defValue)
}