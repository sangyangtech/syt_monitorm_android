package tw.com.sanyangtech.monitorm.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import tw.com.sanyangtech.monitorm.R
import java.lang.Exception



class FirstActivity : AppCompatActivity() {
    private val versionTv by bind<TextView>(R.id.tv_version)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(tw.com.sanyangtech.monitorm.R.layout.activity_first)

        val versionName = try {
            val pInfo = this.getPackageManager().getPackageInfo(packageName, 0)
            pInfo.versionName
        } catch (e: Exception) {
            "unknown"
        }
        versionTv.text = "v $versionName"

        Handler(Looper.getMainLooper()).postDelayed({
            this@FirstActivity.finish()
            this.startActivity(Intent(this@FirstActivity, MainActivity::class.java))
        }, 3000)
    }


}

