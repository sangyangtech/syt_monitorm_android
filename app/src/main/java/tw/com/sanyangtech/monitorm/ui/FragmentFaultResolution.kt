package tw.com.sanyangtech.monitorm.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import tw.com.sanyangtech.monitorm.R
import kotlinx.android.synthetic.main.fault_resolute.*
import tw.com.sanyangtech.monitorm.dto.*
import tw.com.sanyangtech.monitorm.uitls.*
import kotlin.concurrent.thread
import android.os.Looper
import android.os.Handler


class FragmentFaultResolution : Fragment() {


    private val handler = Handler(Looper.getMainLooper())
    private val accountIndex: Int by lazy {
        this.arguments?.getInt(EXTRA_ACCOUNT_INDEX, -1) ?: -1
    }

    private val repairStateSign: TextView by lazy {
        tv_repair_state_sign
    }
    private val commentEt: EditText by lazy {ed_issue_comment_read}
    private val postiveButton: Button by lazy {bt_fixed_positive}
    private val negativeButton: Button by lazy {bt_fixed_negative}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val contentView = inflater.inflate(R.layout.fault_resolute, container, false)
        return contentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val account = GoogleDriveRepo.GetAccountList().get(index = accountIndex)
        setStateSign(account)

        val commentStr = getSharedPreferences(this.context!!, "${account.id}_$SP_KEY_REPAIR_COMMENT", "", SP_NAME_REPAIR)
        commentEt.setText(commentStr, TextView.BufferType.EDITABLE)

        postiveButton.setOnClickListener {
            var activity = this@FragmentFaultResolution.context
            if (activity is MonitorMActivity) {
                activity.showDialog()
            }
            thread(start = true) {
                val updatedResult = updateAccountRepairState(account, REPAIR_STATE_NORMAL, accountIndex)
                if (updatedResult) {
                    val context = this@FragmentFaultResolution.context!!
                    val resources = GoogleDriveRepo.GetResources()
                    val configs = GoogleDriveRepo.GetConfigs()
                    sendNotify(context, account, resources["fault_resolved_notify_title"] ?: "", resources["fault_resolved_notify_content"] ?: "", configs[KEY_RESOURCE_IFTTT_URL] ?: IFTTT_URL_STR)
                    switchToReportPage()
                } else {
                    handler.post {
                        Toast.makeText(activity, R.string.failed_to_execute, Toast.LENGTH_LONG).show()
                    }
                }
                if (activity is MonitorMActivity) {
                    activity.dismissDialog()
                }
            }
        }

        negativeButton.setOnClickListener {
            var activity = this@FragmentFaultResolution.context
            if (activity is MonitorMActivity) {
                activity.showDialog()
            }
            thread(start = true) {
                val repairStateChanged = account.repairState != REPAIR_STATE_STILL_FAULT
                val updatedResult = updateAccountRepairState(account, REPAIR_STATE_STILL_FAULT, accountIndex)
                if (updatedResult) {
                    val context = this@FragmentFaultResolution.context!!
                    val resources = GoogleDriveRepo.GetResources()
                    val configs = GoogleDriveRepo.GetConfigs()
                    sendNotify(context, account, resources["fault_unresolved_notify_title"] ?: "", resources["fault_unresolved_notify_content"] ?: "", configs[KEY_RESOURCE_IFTTT_URL] ?: IFTTT_URL_STR)
                    if (repairStateChanged) {
                        handler.post {
                            setStateSign(account)
                        }
                    }
                } else {
                    handler.post {
                        Toast.makeText(activity, R.string.failed_to_execute, Toast.LENGTH_LONG).show()
                    }
                }
                if (activity is MonitorMActivity) {
                    activity.dismissDialog()
                }
            }
        }

    }

    private fun setStateSign(account: AccountDTO) {
        val backgroundResId: Int
        val textResId: Int
        when (account.repairState) {
            REPAIR_STATE_STILL_FAULT -> {
                backgroundResId = R.drawable.bt_still_fault_sign
                textResId = R.string.still_not_fixed
            }
            REPAIR_STATE_REPORTED -> {
                backgroundResId = R.drawable.bt_reported_sign
                textResId = R.string.reported
            }
            else -> {
                backgroundResId = -1
                textResId = -1
            }
        }
        if (backgroundResId != -1) {
            repairStateSign.setBackgroundResource(backgroundResId)
            repairStateSign.setText(textResId)
        } else {
            repairStateSign.visibility = View.INVISIBLE
        }
    }

    private fun switchToReportPage() {
        val fragmentManager = this.fragmentManager
        val fragment = FragmentReport()
        val bundle = Bundle()
        bundle.putInt(EXTRA_ACCOUNT_INDEX, accountIndex)
        fragment.arguments = bundle
        fragmentManager?.beginTransaction()?.replace(R.id.fragment_container, fragment)?.commit()
    }



}
