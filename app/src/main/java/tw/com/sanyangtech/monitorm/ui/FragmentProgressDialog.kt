package tw.com.sanyangtech.monitorm.ui

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import tw.com.sanyangtech.monitorm.R



class FragmentProgressDialog : DialogFragment() {
//    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//
//
//        setCancelable(false);
//
//        return AlertDialog.Builder(this@FragmentProgressDialog.context!!)
//            .setView(R.layout.progress)
//            // Specifying a listener allows you to take an action before dismissing the dialog.
//            // The dialog is automatically dismissed when a dialog button is clicked.
//
//            // A null listener allows the button to dismiss the dialog and take no further action.
//
//            .show()
//    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.progress, container, true)
        setCancelable(false)
        return view
    }



//    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        val contentView = inflater.inflate(tw.com.sanyangtech.monitorm.R.layout.progress, container, false)
//        return contentView
//
//    }


}