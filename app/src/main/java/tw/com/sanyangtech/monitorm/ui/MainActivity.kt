package tw.com.sanyangtech.monitorm.ui

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v7.app.AppCompatActivity
import tw.com.sanyangtech.monitorm.uitls.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import tw.com.sanyangtech.monitorm.R
import tw.com.sanyangtech.monitorm.dto.*
import java.util.*
import kotlin.concurrent.thread
import java.lang.Exception
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.util.TypedValue


const val TAG = "MonitorM"
const val WHAT_SHOW_ERR_TIP = 1

const val EXTRA_ACCOUNT_INDEX = "account_index"


open class MonitorMActivity : AppCompatActivity() {

    fun showDialog() {
        val supportFragmentManager = supportFragmentManager
        val ft = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            ft.remove(prev)
        }


        val newFragment = FragmentProgressDialog()
//        val bundle = Bundle()
//        bundle.putString("action", "processing")
//        newFragment.arguments = bundle
        newFragment.show(ft, "dialog")
    }

     fun dismissDialog() {
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            val df = prev as DialogFragment
            df.dismiss()
        }
    }

    override fun onBackPressed() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(R.string.sure_to_leave)
            .setCancelable(false)
            .setPositiveButton(R.string.positive, { dialog, id -> this@MonitorMActivity.finish() })
            .setNegativeButton(R.string.negative, { dialog, id -> dialog.cancel() })

        val alert = builder.create()
        alert.show()
        alert.findViewById<TextView>(android.R.id.message)
            ?.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.font_title_size))
        alert.getButton(Dialog.BUTTON_POSITIVE)
            .setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.font_title_size))
        alert.getButton(Dialog.BUTTON_NEGATIVE)
            .setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.font_title_size))

    }
}


class MainActivity : MonitorMActivity() {
    companion object {
        var loginCount = 0;
    }
    private val loginBt by bind<Button>(R.id.bt_login)
    private val accountEt by bind<EditText>(R.id.et_account)
    private val passwordEt by bind<EditText>(R.id.et_password)
    private val versionTv by bind<TextView>(R.id.tv_version)
    private val errTip by bind<TextView>(R.id.tv_err_tip)
    private val handler = object : Handler() {
        override fun handleMessage(msg: Message) {

            when (msg.what) {
                WHAT_SHOW_ERR_TIP -> {
                    if (msg.obj is String) {
                        errTip.visibility = View.VISIBLE
                        errTip.setText(msg.obj.toString())
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(tw.com.sanyangtech.monitorm.R.layout.activity_login)
        loginBt.setOnClickListener {
            showDialog()
            thread(start = true) {
                val id = accountEt.editableText.toString().trim()
                val pwd = passwordEt.editableText.toString().trim();
                checkAvailability(id, pwd)
            }

        }
        val versionName = try {
            val pInfo = this.getPackageManager().getPackageInfo(packageName, 0)
            pInfo.versionName
        } catch (e: Exception) {
            "unknown"
        }
        versionTv.text = "v $versionName"
    }


    private fun checkAvailability(id: String, pwd: String) {
        var errMsg = "";
        if (id.isEmpty()) {
            errMsg = this.getString(R.string.account_enter)
        } else if (pwd.isEmpty()) {
            errMsg = this.getString(R.string.password_enter)
        } else {
            loginCount++
            val iSTC: ISangyangTechCloud = GoogleDriveRepo
            if (loginCount > 1 && iSTC.GetAccountList().isEmpty()) {
                GoogleDriveRepo.reTry()
            }
            val loginAccount = iSTC.GetAccountInfo(id, pwd)
            if (loginAccount != null) {
//                if ((loginAccount.isInvalid == INVALID && loginAccount.identity != IDENTITY_ADMIN) ||
//                    (loginAccount.identity != IDENTITY_ADMIN && loginAccount.identity != IDENTITY_USER)
//                ) {
//                    errMsg = this.getString(R.string.account_invalid)
//                } else {
                    startPage(loginAccount, iSTC.GetAccountList().indexOf(loginAccount))
                    this.finish()
//                }
            } else {
                errMsg = GoogleDriveRepo.GetResources()["login_fail_msg"] ?: this.getString(R.string.login_failed)
            }
        }
        if (!errMsg.isEmpty()) {
            handler.sendMessage(Message.obtain(handler, WHAT_SHOW_ERR_TIP, errMsg))
        }
        handler.post {
            dismissDialog()
        }

    }

    private fun startPage(account: AccountDTO, accountIndex: Int) {
        val openPageIntent = Intent()
        when (account.identity) {
            IDENTITY_ADMIN -> {
                openPageIntent.setClass(this, AdminActivity::class.java)
            }
            IDENTITY_USER -> {
                openPageIntent.putExtra(EXTRA_ACCOUNT_INDEX, accountIndex)
                if (account.isInvalid == INVALID) {
                    Log.i(TAG, "invalid user start user expired page")
                    openPageIntent.setClass(this, ExpiredActivity::class.java)
                } else if (account.expired > Date().time) {
                    Log.i(TAG, "start user page")
                    openPageIntent.setClass(this, UserActivity::class.java)
                } else if (account.repairState == REPAIR_STATE_REPORTED || account.repairState == REPAIR_STATE_STILL_FAULT) {
                    Log.i(TAG, "expired but start user page")
                    openPageIntent.setClass(this, UserActivity::class.java)
                } else {
                    Log.i(TAG, "start user expired page")
                    openPageIntent.setClass(this, ExpiredActivity::class.java)
                }
            }
        }
        this.startActivity(openPageIntent)

    }
}




