package tw.com.sanyangtech.monitorm.ui

import android.content.DialogInterface
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import tw.com.sanyangtech.monitorm.R
import tw.com.sanyangtech.monitorm.dto.AccountDTO
import tw.com.sanyangtech.monitorm.dto.GoogleDriveRepo
import android.content.Intent
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import tw.com.sanyangtech.monitorm.uitls.*
import java.text.SimpleDateFormat
import kotlin.concurrent.thread


class ExpiredActivity : MonitorMActivity() {

    private val extendBtn by bind<Button>(R.id.btn_extend_warranty)
    private val phoneCallBtn by bind<Button>(R.id.bt_tel)
    private val emailBtn by bind<Button>(R.id.bt_email)

    private val handler by lazy { Handler(Looper.getMainLooper()) }
    private val accountIndex: Int by lazy {
        this.intent.getIntExtra(EXTRA_ACCOUNT_INDEX, -1);
    }


    private val account: AccountDTO by lazy {
        GoogleDriveRepo.GetAccountList().get(accountIndex)
    }
    private val expiredTv by bind<TextView>(R.id.tv_expired)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.setContentView(R.layout.out_warranty)

        emailBtn.setOnClickListener {
            val uri = Uri.parse("mailto:${GoogleDriveRepo.GetConfigs()[KEY_RESOURCE_EMAIL]}")
            val intent = Intent(Intent.ACTION_SENDTO, uri)
            this.startActivity(intent)
        }
        phoneCallBtn.setOnClickListener {
            val uri = Uri.parse("tel:${GoogleDriveRepo.GetConfigs()[KEY_RESOURCE_TEL]}")
            val intent = Intent(Intent.ACTION_DIAL, uri)
            this.startActivity(intent)
        }
        extendBtn.setOnClickListener {
            showDialog()
            thread(true) {
                val resource = GoogleDriveRepo.GetResources()
                val configs = GoogleDriveRepo.GetConfigs()
                val result = sendNotify(
                    this@ExpiredActivity,
                    account,
                    resource["extend_notify_title"] ?: "",
                    resource["extend_notify_content"] ?: "",
                    configs[KEY_RESOURCE_IFTTT_URL] ?: IFTTT_URL_STR
                )
                if (!result) {
                    handler.post {
                        Toast.makeText(this@ExpiredActivity, R.string.send_extend_failed, Toast.LENGTH_LONG).show()
                    }
                } else {
                    val msg = this@ExpiredActivity.getString(R.string.send_extend_success)
                    val positiveBtnStr = this@ExpiredActivity.getString(R.string.yes)
                    handler.post {
                        showAlertDialog(
                            this@ExpiredActivity,
                            null,
                            msg,
                            positiveBtnStr,
                            DialogInterface.OnClickListener { dialog, whichButton -> },
                            null,
                            null
                        )
                    }
                }
                dismissDialog()
            }
        }

        expiredTv.setText(SimpleDateFormat(EXPIRED_DATE_FORMAT).format(account.expired))


    }
}
