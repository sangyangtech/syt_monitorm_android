package tw.com.sanyangtech.monitorm.ui

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.app.Fragment
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.fault_report.*
import tw.com.sanyangtech.monitorm.R
import tw.com.sanyangtech.monitorm.dto.GoogleDriveRepo
import android.widget.ArrayAdapter
import tw.com.sanyangtech.monitorm.dto.REPAIR_STATE_REPORTED
import tw.com.sanyangtech.monitorm.uitls.*
import kotlin.concurrent.thread


class FragmentReport: Fragment() {

    private val accountIndex: Int by lazy {
        this.arguments?.getInt(EXTRA_ACCOUNT_INDEX, -1) ?: -1
    }

    private val handler = Handler(Looper.getMainLooper())

    private val products: List<String> by lazy {
        GoogleDriveRepo.GetFaultReasons().values.toList()
    }

    private val productSpinner: Spinner by lazy { sp_product_type }
    private val reportButton: Button by lazy { bt_report }
    private val commentEt: EditText by lazy {ed_issue_comment}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fault_report, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val account = GoogleDriveRepo.GetAccountList().get(index = accountIndex)

        // set spinner
        val productAdapter = ArrayAdapter<String>(
            this.context,
            R.layout.spinner_item,
            products
        )
        productSpinner.adapter = productAdapter

        reportButton.setOnClickListener {
            var activity = this@FragmentReport.context
            if (activity is MonitorMActivity) {
                activity.showDialog()
            }
            thread(start = true) {
                val comment = commentEt.text.toString().trim()
                val product = productSpinner.selectedItem.toString().trim()

                val updatedResult = updateAccountRepairState(account, REPAIR_STATE_REPORTED, accountIndex)
                if (updatedResult) {
                    val context = this@FragmentReport.context!!
                    val resources = GoogleDriveRepo.GetResources()
                    val configs = GoogleDriveRepo.GetConfigs()
                    setSharedPreferences(context, "${account.id}_$SP_KEY_REPAIR_COMMENT", comment, SP_NAME_REPAIR)
                    setSharedPreferences(context, "${account.id}_$SP_KEY_REPAIR_PRODUCT", product, SP_NAME_REPAIR)
                    sendNotify(context, account, resources["booking_notify_title"] ?: "", resources["booking_notify_content"] ?: "", configs[KEY_RESOURCE_IFTTT_URL] ?: IFTTT_URL_STR)
                    switchToResolutePage()
                } else {
                    handler.post {
                        Toast.makeText(activity, R.string.failed_to_execute, Toast.LENGTH_LONG).show()
                    }
                }
                if (activity is MonitorMActivity) {
                    activity.dismissDialog()
                }
            }
        }

    }

    private fun switchToResolutePage() {
        val fragmentManager = this.fragmentManager
        val fragment = FragmentFaultResolution()
        val bundle = Bundle()
        bundle.putInt(EXTRA_ACCOUNT_INDEX, accountIndex)
        fragment.arguments = bundle
        fragmentManager?.beginTransaction()?.replace(R.id.fragment_container, fragment)?.commit()
    }


}
