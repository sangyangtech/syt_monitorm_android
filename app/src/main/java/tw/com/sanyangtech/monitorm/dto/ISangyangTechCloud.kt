package tw.com.sanyangtech.monitorm.dto

interface ISangyangTechCloud {
    fun GetAccountInfo(id: String, pwd: String): AccountDTO?
    fun GetResources(): HashMap<String, String>
    fun GetConfigs(): HashMap<String, String>
    fun GetFaultReasons(): HashMap<String, String>
    fun GetAccountList(): List<AccountDTO>
}