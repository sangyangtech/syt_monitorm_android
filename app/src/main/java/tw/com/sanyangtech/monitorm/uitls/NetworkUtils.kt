package tw.com.sanyangtech.monitorm.uitls

import android.content.Context
import tw.com.sanyangtech.monitorm.uitls.Log
//import org.apache.commons.mail.DefaultAuthenticator
//import org.apache.commons.mail.HtmlEmail
import org.json.JSONObject
import tw.com.sanyangtech.monitorm.dto.AccountDTO
import tw.com.sanyangtech.monitorm.dto.GoogleDriveRepo
import tw.com.sanyangtech.monitorm.ui.TAG
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

fun sendNotify(context: Context, account: AccountDTO, titleFormat: String, contentFormat: String, iftttUrl: String = IFTTT_URL_STR): Boolean {
    val comment = getSharedPreferences(context, "${account.id}_$SP_KEY_REPAIR_COMMENT", "", SP_NAME_REPAIR)
    val product = getSharedPreferences(context, "${account.id}_$SP_KEY_REPAIR_PRODUCT", "", SP_NAME_REPAIR)
    val title = replaceSpecifier(titleFormat, account.id, product, comment)
    val content = replaceSpecifier(contentFormat, account.id, product, comment)
    val sendMsgResult = sendMessage(iftttUrl, content)
    val sendGmailResult = sendGmail(URL_SEND_MAIL, GoogleDriveRepo.GetConfigs()[KEY_RESOURCE_RECEIVERS]?: "", title, content)
    Log.i(TAG,"sendMsgResult:$sendMsgResult sendGmailResult:$sendGmailResult" )
    return sendMsgResult && sendGmailResult
}


//IFTTT service
fun sendMessage(iftttUrl: String = IFTTT_URL_STR, vararg parameters: String): Boolean {
    var result = false
    try {
        val urlObj = URL(iftttUrl)
        Log.v(TAG, urlObj.toString())
        val con = urlObj.openConnection() as HttpURLConnection
        con.setRequestProperty("Content-Type", "application/json; charset=utf-8")
        con.requestMethod = "POST"
        val os = con.outputStream
        val requestJson = JSONObject()
        for ((idx, paramter) in parameters.withIndex()) {
            val key = IFTTT_JSON_KEY_FORMAT.format((idx + 1).toString())
            requestJson.put(key, paramter)
        }
        Log.v(TAG, "request json: ${requestJson.toString(4)}")
        os.write(requestJson.toString().toByteArray(charset("UTF-8")))
        val resultCode = con.responseCode
        Log.v(TAG, "result:$resultCode msg:${con.responseMessage}")
        if (resultCode == HttpURLConnection.HTTP_OK) {
            result = true
            val inputStream = con.inputStream
            val reader = BufferedReader(InputStreamReader(inputStream))
            val resultStr = reader.use { it.readText() }
            Log.e(TAG, "IFTTT:$resultStr")
        } else {
            Log.e(TAG, "IFTTT:failed")
        }
    } catch (e: Exception) {
        e.printStackTrace();
    }
    return result

}


fun sendGmail(url: String = URL_SEND_MAIL, to: String, subject: String, content: String): Boolean {
    Log.v(TAG, "$to  $subject   $content")
    var result = false
    try {
        val urlObj = URL("$url?to=$to&subject=$subject&content=$content")
        Log.v(TAG, urlObj.toString())
        val con = urlObj.openConnection() as HttpURLConnection
        con.setRequestProperty("Content-Type", "application/json; charset=utf-8")
        con.requestMethod = "GET"
        val resultCode = con.responseCode
        Log.v(TAG, "result:$resultCode msg:${con.responseMessage}")
        if (resultCode == HttpURLConnection.HTTP_OK) {
            result = true
            Log.e(TAG, "send mail success")
        } else {
            Log.e(TAG, "IFTTT:failed")
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return result
}


fun replaceSpecifier(str: String, account: String, product: String, comment: String) = str.replace(SPECIFIER_ACCOUNT, account).replace(SPECIFIER_PRODUCT, product).replace(SPECIFIER_ERROR_REASON, comment)
