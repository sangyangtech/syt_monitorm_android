package tw.com.sanyangtech.monitorm.ui

import android.app.Activity
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.view.View


const val EXPIRED_DATE_FORMAT = "yyyy-MM-dd"

fun <T : View> Activity.bind(@IdRes res: Int): Lazy<T> {
    return lazy { findViewById<T>(res) }
}


fun <T : View> View.bind(@IdRes res: Int): Lazy<T> {
    return lazy { findViewById<T>(res) }
}

fun <T : View> Fragment.bind(root: View, @IdRes res: Int): Lazy<T> {
    return lazy { root.findViewById(res) as T }
}

