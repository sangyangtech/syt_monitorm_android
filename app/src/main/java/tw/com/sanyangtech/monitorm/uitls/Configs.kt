package tw.com.sanyangtech.monitorm.uitls

const val URL_SEND_MAIL = "https://script.google.com/macros/s/AKfycby698eZOrce868Utzd768ziTdTL2vKml_MSj3Ou0fjzk3d-HGGi/exec"

const val IFTTT_URL_STR = "https://maker.ifttt.com/trigger/fireE/with/key/du_zzywPZIcc38REXi-fMR"
const val IFTTT_JSON_KEY_FORMAT = "value%s"


const val SP_NAME_REPAIR =  "repair"

const val SP_KEY_REPAIR_COMMENT =  "repair_comment"
const val SP_KEY_REPAIR_PRODUCT =  "repair_product"

const val KEY_RESOURCE_IFTTT_URL = "ifttt_url"
const val KEY_RESOURCE_EMAIL = "cs_email"
const val KEY_RESOURCE_TEL = "cs_tel"
const val KEY_RESOURCE_RECEIVERS = "receivers"


const val SPECIFIER_PRODUCT = "{Product Category}"
const val SPECIFIER_ACCOUNT = "{Account}"
const val SPECIFIER_ERROR_REASON = "{Error Reason}"
