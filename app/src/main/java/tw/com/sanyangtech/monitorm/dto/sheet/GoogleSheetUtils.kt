package tw.com.sanyangtech.monitorm.uitls

import tw.com.sanyangtech.monitorm.uitls.Log
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONObject
import tw.com.sanyangtech.monitorm.ui.TAG
import tw.com.sanyangtech.monitorm.dto.AccountDTO
import tw.com.sanyangtech.monitorm.dto.sheet.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*


fun inquiryGoogleSheet(
    spreadsheetId: String, sheetName: String = "工作表1",
    rangeFrom: String, rangeTo: String, key: String = GOOGLESHEET_API_KEY
): JSONObject {
    var result = JSONObject()
    try {
        val urlObj = URL(URL_GET_SHEET_FORMAT.format(spreadsheetId, sheetName, rangeFrom, rangeTo, key))
        Log.v(TAG, urlObj.toString())
        val con = urlObj.openConnection() as HttpURLConnection
        con.requestMethod = "GET"
        val resultCode = con.responseCode
        Log.v(TAG, "result:$resultCode msg:${con.responseMessage}")
        val inputStream = if (resultCode == 200) con.inputStream else con.errorStream
        val reader = BufferedReader(InputStreamReader(inputStream))
        val jsonString = reader.use { it.readText() }
        result = JSONObject(jsonString)
        Log.v(TAG, result.toString(4))
    } catch (e: Exception) {
        e.printStackTrace()
        Log.e(TAG, e.message)
    }
    return result
}


fun getAccountTitle(result: JSONObject): MutableList<String> {
    val accountTitles = ArrayList<String>()
    if (result.has(JKEY_VALUES)) {
        val rowArray = result.getJSONArray(JKEY_VALUES)
        if (rowArray.length() > 0) {
            val row = rowArray.getJSONArray(0)
            var title: String
            for (idx in 0 until row.length()) {
                title = row.getString(idx)
                Log.v(TAG, "account_title:${title}")
                accountTitles.add(title)
            }
        }
    } else {
        Log.e(TAG, "can't get account title")
    }
    return accountTitles
}

private fun getAccount(row: JSONArray): AccountDTO {
    try{
        val dateTimeFormatter = SimpleDateFormat(GOOGLE_SHEET_DATE_TIME_FORMAT,  Locale("zh", "TW"))
        val dateFormatter = SimpleDateFormat(GOOGLE_SHEET_DATE_FORMAT)

        val id = row.getString(0)
        val pwd = row.getString(1)
        // for empty expired case, like admin
        val expired = if (row.getString(ACCOUNT_DATE_EXPIRED_IDX).isEmpty()) 0L else dateFormatter.parse(row.getString(ACCOUNT_DATE_EXPIRED_IDX)).time


        val isInvalid = row.getInt(ACCOUNT_INT_VALID_IDX)
        val repairState = row.getInt(ACCOUNT_INT_REPAIR_STATE_IDX)
        val cDate = dateTimeFormatter.parse(row.getString(ACCOUNT_DATE_CDATE_IDX)).time
        val mDate = dateTimeFormatter.parse(row.getString(ACCOUNT_DATE_MDATE_IDX)).time
        val identity = row.getString(7)
        val comment = if (row.length() == ACCOUNT_COLUMN_SIZE) row.getString(8) else ""
        val accountDTO = AccountDTO(id, pwd, expired, isInvalid
            , repairState, cDate, mDate, identity, comment)
        Log.v(TAG, accountDTO.toString())
        return accountDTO
    } catch (e: Exception) {
        e.printStackTrace()
        Log.e(TAG, "get Account failed, return virtual account")
        return AccountDTO()
    }
}


fun getAccountList(result: JSONObject): MutableList<AccountDTO> {
    val accountList = ArrayList<AccountDTO>()
    if (result.has(JKEY_VALUES)) {
        val rowArray = result.getJSONArray(JKEY_VALUES)
        for (index in 1 until rowArray.length()) {
            if (index != 0) {
                val row = rowArray.getJSONArray(index)
                // comment could be null
                if (row.length() >= ACCOUNT_COLUMN_SIZE - 1 ) {
                    accountList.add(getAccount(row))
                } else {
                    Log.e(TAG, "Account table was confused")
                }
            }
        }
    } else {


        Log.e(TAG, "can't get account rows")
    }
    return accountList
}

fun getPairs(result: JSONObject): HashMap<String, String> {
    val pairs = HashMap<String, String>()
    if (result.has(JKEY_VALUES)) {
        val rowArray = result.getJSONArray(JKEY_VALUES)
        for (index in 1 until rowArray.length()) {
            val row = rowArray.getJSONArray(index)
            if (row.length() >= 2) {
                val key = row.getString(0)
                val value = row.getString(1)
                Log.v(TAG, "key:$key  value:$value")
                pairs[key] = value
            } else {
                Log.e(TAG, "config table was confused")
            }
        }
    } else {
        Log.e(TAG, "can't get config")
    }
    return pairs
}

fun updateAccountRepairState(account: AccountDTO, repairState: Int, accountIndex: Int): Boolean {
    val accountToUpdate = account.copy()
    accountToUpdate.mDate = Date().time
    accountToUpdate.repairState = repairState

    val jsonObject = JSONObject(Gson().toJson(accountToUpdate))

    //add idx key for apps script reference
    jsonObject.put(JKEY_ACCOUNT_IDX, accountIndex)
    transferToStrDate(jsonObject, accountToUpdate)

    val result = updateGoogleSheetAccount(jsonObject)
    if (result) {
        account.mDate = accountToUpdate.mDate
        account.repairState = accountToUpdate.repairState
    }
    return result
}

fun updateAccount(account: AccountDTO, id: String, pwd: String, warrantyDate: Long, isInvalid: Int, comment: String, accountIndex: Int): Boolean {
    val accountToUpdate = account.copy()
    accountToUpdate.mDate = Date().time
    accountToUpdate.id = id
    accountToUpdate.pwd = pwd
    accountToUpdate.expired = warrantyDate
    accountToUpdate.isInvalid = isInvalid
    accountToUpdate.comment = comment

    val jsonObject = JSONObject(Gson().toJson(accountToUpdate))

    //add idx key for apps script reference
    jsonObject.put(JKEY_ACCOUNT_IDX, accountIndex)
    transferToStrDate(jsonObject, accountToUpdate)

    val result = updateGoogleSheetAccount(jsonObject)
    if (result) {
        account.mDate = accountToUpdate.mDate
        account.id = accountToUpdate.id
        account.pwd = accountToUpdate.pwd
        account.expired = accountToUpdate.expired
        account.isInvalid = accountToUpdate.isInvalid
        account.comment = accountToUpdate.comment
    }
    return result
}




fun updateGoogleSheetAccount(accountObject: JSONObject): Boolean {
    var updatedResult = false
    try {
        val urlObj = URL(tw.com.sanyangtech.monitorm.dto.sheet.URL_UPDATE_ACCOUNT)
        Log.v(TAG, "update account row ${urlObj.toString()}")
        val con = urlObj.openConnection() as HttpURLConnection
        con.setRequestProperty("Content-Type", "application/json; charset=utf-8")
        con.requestMethod = "POST"
        val os = con.outputStream
        Log.v(TAG, "request json: ${accountObject.toString()}")
        os.write(accountObject.toString().toByteArray(charset("UTF-8")))
        val resultCode = con.responseCode
        Log.v(TAG, "result:$resultCode msg:${con.responseMessage}")
        if (resultCode == HttpURLConnection.HTTP_OK) {
            updatedResult = true
        }
    } catch (e: Exception) {
        e.printStackTrace()
        Log.e(TAG, e.message)
    }
    return updatedResult
}


fun transferToStrDate (accountObject: JSONObject, accountToUpdate: AccountDTO) {
    val dateTimeFormatter = SimpleDateFormat(GOOGLE_SHEET_DATE_TIME_FORMAT, Locale("zh", "TW"))
    val dateFormatter = SimpleDateFormat(GOOGLE_SHEET_DATE_FORMAT)
    accountObject.put(ACCOUNT_KEY_MDATE, dateTimeFormatter.format(accountToUpdate.mDate))
    accountObject.put(ACCOUNT_KEY_CDATE, dateTimeFormatter.format(accountToUpdate.cDate))
    accountObject.put(ACCOUNT_EXPIRED_DATE, dateFormatter.format(accountToUpdate.expired))
}
//fun updateGoogleSheetField(
//    spreadsheetId: String, sheetName: String = "工作表1",
//    rangeFrom: String, rangeTo: String, values: Array<Array<String>>, key: String = GOOGLESHEET_API_KEY
//): JSONObject {
//    var result = JSONObject()
//    try {
//        val urlObj = URL(URL_UPDATE_SHEET_FORMAT.format(spreadsheetId, sheetName, rangeFrom, rangeTo))
//        Log.v(TAG, urlObj.toString())
//        val con = urlObj.openConnection() as HttpURLConnection
//        con.setRequestProperty("Content-Type", "application/json; charset=utf-8")
//        con.requestMethod = "POST"
//        val valueRange = ValueRange(ValueRange.getRange(sheetName, rangeFrom, rangeTo), values)
//        val os = con.outputStream
//        Log.i(TAG, "request json: ${Gson().toJson(valueRange)}")
//        os.write(Gson().toJson(valueRange).toByteArray(charset("UTF-8")))
//        val resultCode = con.responseCode
//        Log.w(TAG, "result:$resultCode msg:${con.responseMessage}")
//        val inputStream = if (resultCode == 200) con.inputStream else con.errorStream
//        val reader = BufferedReader(InputStreamReader(inputStream))
//        val jsonString = reader.use { it.readText() }
//        result = JSONObject(jsonString)
//        Log.i(TAG, result.toString(4))
//    } catch (e: Exception) {
//        e.printStackTrace()
//        Log.e(TAG, e.message)
//    }
//    return result
//}

