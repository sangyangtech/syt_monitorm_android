package tw.com.sanyangtech.monitorm.dto

import tw.com.sanyangtech.monitorm.uitls.getAccountList
import tw.com.sanyangtech.monitorm.uitls.getAccountTitle
import tw.com.sanyangtech.monitorm.uitls.getPairs
import tw.com.sanyangtech.monitorm.uitls.inquiryGoogleSheet

const val SPREADSHEET_ID_ACCOUNT = "1sP6ik2tJm_aZM2R0-67LHCs1oc2MAc1YwgejhD62t-M"
const val SPREADSHEET_ID_FAULT_REASON = "1nq7CU3EejgcGaVe9AT8_wsHpcFfGwY2jCEVA5yxVcjo"
const val SPREADSHEET_ID_RESOURCE = "1_QL5GePvswygq7labslJIwk56_745l2O8mDC_aavk8Y"
const val SPREADSHEET_ID_CONFIG = "1jzKTZojKUmy1r10VSVSvngN7RscNnoqFYynib1JROPk"

object GoogleDriveRepo : ISangyangTechCloud {

    private val accountList: MutableList<AccountDTO>
    private val accountTitles: MutableList<String>
    private val configs: HashMap<String, String>
    private val faultReasons: HashMap<String, String>
    private val resources: HashMap<String, String>

    init {
        val result = inquiryGoogleSheet(SPREADSHEET_ID_ACCOUNT, rangeFrom = "A", rangeTo = "I")
        accountList = getAccountList(result)
        accountTitles = getAccountTitle(result)

        val configResult = inquiryGoogleSheet(SPREADSHEET_ID_CONFIG, rangeFrom = "A", rangeTo = "B")
        configs = getPairs(configResult)

        val faultReasonsResult = inquiryGoogleSheet(SPREADSHEET_ID_FAULT_REASON, rangeFrom = "A", rangeTo = "B")
        faultReasons = getPairs(faultReasonsResult)

        val resourceResult = inquiryGoogleSheet(SPREADSHEET_ID_RESOURCE, rangeFrom = "A", rangeTo = "B")
        resources = getPairs(resourceResult)
    }

    fun reTry() {

        val result = inquiryGoogleSheet(SPREADSHEET_ID_ACCOUNT, rangeFrom = "A", rangeTo = "I")
        accountList.addAll(getAccountList(result))
        accountTitles.addAll(getAccountTitle(result))

        val configResult = inquiryGoogleSheet(SPREADSHEET_ID_CONFIG, rangeFrom = "A", rangeTo = "B")
        configs.putAll(getPairs(configResult))

        val faultReasonsResult = inquiryGoogleSheet(SPREADSHEET_ID_FAULT_REASON, rangeFrom = "A", rangeTo = "B")
        faultReasons.putAll(getPairs(faultReasonsResult))

        val resourceResult = inquiryGoogleSheet(SPREADSHEET_ID_RESOURCE, rangeFrom = "A", rangeTo = "B")
        resources.putAll(getPairs(resourceResult))
    }


//    private val faultReasons by lazy {
//        val result = inquiryGoogleSheet(SPREADSHEET_ID_FAULT_REASON, rangeFrom = "A", rangeTo = "B")
//        getPairs(result)
//    }
//
//    private val resources by lazy {
//        val result = inquiryGoogleSheet(SPREADSHEET_ID_RESOURCE, rangeFrom = "A", rangeTo = "B")
//        getPairs(result)
//    }


    override fun GetAccountInfo(id: String, pwd: String): AccountDTO? {
        for (candidateAccount in accountList) {
            if (candidateAccount.id == id && candidateAccount.pwd == pwd) {
                return candidateAccount
            }
        }
        return null
    }

    override fun GetResources() = resources

    override fun GetConfigs() = configs

    override fun GetFaultReasons() = faultReasons

    override fun GetAccountList() = accountList

}