package tw.com.sanyangtech.monitorm.dto.sheet




const val JKEY_ERROR = "error"
const val JKEY_VALUES = "values"

const val GOOGLE_SHEET_DATE_TIME_FORMAT = "yyyy/M/d a h:mm:ss"
const val GOOGLE_SHEET_DATE_FORMAT = "yyyy/M/d"

//all about account table
const val ACCOUNT_COLUMN_SIZE = 9
const val ACCOUNT_DATE_EXPIRED_IDX = 2
const val ACCOUNT_INT_VALID_IDX = 3
const val ACCOUNT_INT_REPAIR_STATE_IDX = 4
const val ACCOUNT_DATE_CDATE_IDX = 5
const val ACCOUNT_DATE_MDATE_IDX = 6


const val ACCOUNT_EXPIRED_DATE = "expired"
const val ACCOUNT_KEY_MDATE = "mDate"
const val ACCOUNT_KEY_CDATE = "cDate"


const val JKEY_ACCOUNT_IDX = "idx"

const val URL_GET_SHEET_FORMAT = "https://sheets.googleapis.com/v4/spreadsheets/%s/values/%s!%s:%s?key=%s"
const val URL_UPDATE_SHEET_FORMAT = "https://sheets.googleapis.com/v4/spreadsheets/%s/values/%s!%s:%s"
const val URL_UPDATE_ACCOUNT = "https://script.google.com/macros/s/AKfycbycKpCOOpNrKXPEQDbn87d62qDD19_pzzp6ZuuIqtZvZPRYAjc/exec"

const val GOOGLESHEET_API_KEY = "AIzaSyDPxUWHynP9YzHXqa0YIztgOn8shFttgp8"
